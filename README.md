# README #

Maven Artifact Repository for Olympus Project

To include this artifact in project add following configuration pom.xml

```xml

<!-- Add Repository -->

  <repositories>
    <repository>
      <id>synergian-repo</id>
      <!-- <url>git:master://git@bitbucket.org:ply-labs/olympus-artifact.git</url> -->
      <url>https://api.bitbucket.org/1.0/repositories/ply-labs/olympus-artifact.git/raw/master</url>
    </repository>
  </repositories>

<!-- Add Dependency -->

    <dependency>
      <groupId>com.plylabs</groupId>
      <artifactId>olympus</artifactId>
      <version>1.0</version>
    </dependency>

  
```